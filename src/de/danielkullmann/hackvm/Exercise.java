package de.danielkullmann.hackvm;

import java.util.ArrayList;
import java.util.List;

public class Exercise {

	private String id;
	private String description;
	private String illegalOpcodes;
	private List<SolutionCheck> solutionChecks = new ArrayList<SolutionCheck>();
	private int categoryId;
	private String categoryName;
	
	public Exercise(String description, List<SolutionCheck> solutionChecks, String illegalOpcodes) {
		super();
		this.description = description;
		this.solutionChecks = solutionChecks;
		this.illegalOpcodes = illegalOpcodes;
	}
	
	public String getDescription() {
		return description;
	}
	public List<SolutionCheck> getSolutionChecks() {
		return solutionChecks;
	}

	public boolean checkProgram( String program )  {
		for (SolutionCheck solutionCheck : getSolutionChecks()) {
			if ( ! solutionCheck.check(program) ) return false;
		}
		return true;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setSolutionChecks(List<SolutionCheck> solutionChecks) {
		this.solutionChecks = solutionChecks;
	}

	public String getIllegalOpcodes() {
		return illegalOpcodes;
	}

	public void setIllegalOpcodes(String illegalOpcodes) {
		this.illegalOpcodes = illegalOpcodes;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

}
