package de.danielkullmann.hackvm;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;


public class HackUtil {

	public static List<Exercise> loadExerciseXMLs() throws IOException {
		URL url = HackUtil.class.getClassLoader().getResource( "de/danielkullmann/hackvm/data/exercises.xml" );
		InputStream is = url.openStream();
		SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            ExerciseReader handler = new ExerciseReader();
			parser.parse( is, handler );
			return handler.getExercises();
        } catch (ParserConfigurationException e) {
        	e( "Error when parsing exercises.xml", e );
        } catch (SAXException e) {
        	e( "Error when parsing exercises.xml", e );
        }
        return null;
	}

	public static void e(String msg, Throwable e) {
		System.out.println( "ERROR: " + msg );
		e.printStackTrace();
	}

	public static void d(String msg) {
		System.out.println( "DEBUG: " + msg );
	}

}
