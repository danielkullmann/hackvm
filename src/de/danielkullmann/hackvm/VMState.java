package de.danielkullmann.hackvm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import de.danielkullmann.hackvm.HackVM.Trace;

public class VMState {

	private static final int TOP_STACK_SIZE = 16;

	private Stack<Integer> stack = new Stack<Integer>();
	private String error = null;
	private StringBuilder output = new StringBuilder();
	private int programCounter = 0;
	private int ticks = 0;
	private HashMap<Integer, Integer> memory = new HashMap<Integer, Integer>();
	private ArrayList<Trace> executionTrace = new ArrayList<Trace>();

	
	public void tick() {
		this.ticks++;
	}
	
	public void addTrace(Trace trace) {
		this.executionTrace.add( trace );
	}

	public Stack<Integer> topStack() {
		Stack<Integer> result = new Stack<Integer>();
		int startIndex = 0;
		if ( stack.size() > TOP_STACK_SIZE ) {
			startIndex = stack.size() - TOP_STACK_SIZE;
		}
		while ( startIndex < stack.size() ) {
			result.add( stack.get(startIndex) );
			startIndex++;
		}
		return result;
	}
	
	public Stack<Integer> getStack() {
		return stack;
	}
	public void setStack(Stack<Integer> stack) {
		this.stack = stack;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getOutput() {
		return output.toString();
	}
	public void setOutput(StringBuilder output) {
		this.output = output;
	}
	public int getProgramCounter() {
		return programCounter;
	}
	public void setProgramCounter(int programCounter) {
		this.programCounter = programCounter;
	}
	public int getTicks() {
		return ticks;
	}
	public void setTicks(int ticks) {
		this.ticks = ticks;
	}
	public HashMap<Integer, Integer> getMemory() {
		return memory;
	}
	public void setMemory(HashMap<Integer, Integer> memory) {
		this.memory = memory;
	}
	public ArrayList<Trace> getExecutionTrace() {
		return executionTrace;
	}
	public void setExecutionTrace(ArrayList<Trace> executionTrace) {
		this.executionTrace = executionTrace;
	}

	public void output(Character ch) {
		this.output.append( ch );
	}

	public void output(Integer i) {
		this.output.append( i );
	}
	
}
