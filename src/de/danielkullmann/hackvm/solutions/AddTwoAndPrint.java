package de.danielkullmann.hackvm.solutions;

import java.util.Random;
import java.util.Stack;

import de.danielkullmann.hackvm.HackVM;
import de.danielkullmann.hackvm.SolutionCheck;
import de.danielkullmann.hackvm.VMState;

public class AddTwoAndPrint implements SolutionCheck {

	@Override
	public boolean check(String program) {
		Random r = new Random();
		HackVM vm = new HackVM();
		for (int i = 0; i < 100; i++) {
			int n1 = r.nextInt();
			int n2 = r.nextInt();
			Stack<Integer> stack = HackVM.parseStack( n1 + " " + n2 );
			VMState result = vm.execute(program, stack, null);
			if ( ! result.getOutput().equals( "" + (n1+n2) ) ) return false;
		}
		return true;
	}

}
