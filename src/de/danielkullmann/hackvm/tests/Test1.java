package de.danielkullmann.hackvm.tests;

import junit.framework.TestCase;
import de.danielkullmann.hackvm.HackVM;
import de.danielkullmann.hackvm.VMState;

public class Test1 extends TestCase {

	public void test1() {
		HackVM vm = new HackVM();

		VMState result = vm.execute( " ", "", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 0 );

		result = vm.execute( "p", "1", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 0 );
		assertTrue( result.getOutput().equals( "1" ) );

		result = vm.execute( "P", "65", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 0 );
		assertTrue( result.getOutput().equals( "A" ) );

		for (int i = 0; i < 10; i++) {
			result = vm.execute( ""+i, "", null );
			assertNull( result.getError() );
			assertTrue( result.getStack().size() == 1 );
			assertTrue( result.getStack().pop() == i );
		}

		result = vm.execute( "+", "7, 8", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 15 );

		result = vm.execute( "-", "7, 8", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == -1 );

		result = vm.execute( "*", "7, 8", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 56 );

		result = vm.execute( "/", "6, 3", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 2 );
		
		result = vm.execute( ":", "6, 3", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 1 );
		
		result = vm.execute( ":", "3, 6", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == -1 );
		
		result = vm.execute( ":", "8, 8", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 0 );
		
		result = vm.execute( "g!1", "2", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 1 );

		result = vm.execute( "g!1", "1", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 0 );

		result = vm.execute( "?!1", "0, 2", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 1 );

		result = vm.execute( "?!1", "1, 2", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 0 );

		result = vm.execute( "      c1!", "8", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 6 );

		result = vm.execute( "      $1!", "8", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 0 );

		result = vm.execute( "<", "1", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 0 );

		result = vm.execute( "><", "1, 2, 1", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 2 );
		
		result = vm.execute( "^", "4, 0", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 2 );
		assertTrue( result.getStack().pop() == 4 );
		assertTrue( result.getStack().pop() == 4 );

		result = vm.execute( "^", "1, 2, 3, 4, 2", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 5 );
		assertTrue( result.getStack().pop() == 2 );
		assertTrue( result.getStack().pop() == 4 );
		assertTrue( result.getStack().pop() == 3 );
		assertTrue( result.getStack().pop() == 2 );
		assertTrue( result.getStack().pop() == 1 );

		result = vm.execute( "v", "4, 0", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 1 );
		assertTrue( result.getStack().peek() == 4 );

		result = vm.execute( "v", "1, 2, 3, 4, 2", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 4 );
		assertTrue( result.getStack().pop() == 2 );
		assertTrue( result.getStack().pop() == 4 );
		assertTrue( result.getStack().pop() == 3 );
		assertTrue( result.getStack().pop() == 1 );
	
		result = vm.execute( "d", "1", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 0 );

		result = vm.execute( "ddddd", "1, 2, 3, 4, 5, 6, 7, 8, 9", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 4 );
		assertTrue( result.getStack().pop() == 4 );
		assertTrue( result.getStack().pop() == 3 );
		assertTrue( result.getStack().pop() == 2 );
		assertTrue( result.getStack().pop() == 1 );

		result = vm.execute( "!11111", "", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 0 );

		result = vm.execute( "11!111", "", null );
		assertNull( result.getError() );
		assertTrue( result.getStack().size() == 2 );

	}
}
