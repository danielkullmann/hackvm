package de.danielkullmann.hackvm.tests;

import java.util.List;

import junit.framework.TestCase;
import de.danielkullmann.hackvm.Exercise;
import de.danielkullmann.hackvm.HackUtil;

public class ReadExercises extends TestCase {

	public void testReading() throws Exception {
		List<Exercise> result = HackUtil.loadExerciseXMLs();
		assertNotNull( result );
		assertTrue( result.size() >= 1 );
		for (Exercise exercise : result) {
			assertNotNull( exercise );
			assertNotNull( exercise.getDescription() );
			assertNotNull( exercise.getSolutionChecks() );
			assertTrue( exercise.getSolutionChecks().size() >= 1 );
		}
	}
	
	public void testSolution() throws Exception {
		String[][] solutions = new String[][] {
				new String[] {"b59d189b-79d0-4f9b-9838-35c10f657269", "+p"}
		};
		List<Exercise> result = HackUtil.loadExerciseXMLs();
		for (Exercise exercise : result) {
			for (int i = 0; i < solutions.length; i++) {
				if ( solutions[i][0].equals( exercise.getId() ) ) {
					exercise.checkProgram( solutions[i][1] );
				}
				
			}
		}
	}

}
