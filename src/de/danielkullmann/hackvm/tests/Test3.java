package de.danielkullmann.hackvm.tests;

import java.util.HashMap;

import junit.framework.TestCase;
import de.danielkullmann.hackvm.HackVM;
import de.danielkullmann.hackvm.VMState;

public class Test3 extends TestCase {

	public void testFibonacci() {
		HackVM vm = new HackVM();
		VMState result = vm.execute("1p 48*P 1p 48*P 2p 48*P 3p 48*P 5p", "", "");
		assertNull( result.getError() );
		assertEquals( result.getOutput(), "1 1 2 3 5" );
	}

	public void testReverseList() {
		HackVM vm = new HackVM();
		//                           setup   loop loop end?      copy one
		VMState result = vm.execute("0^2v+1- 1^1^ 1^1^:0^1-*55*? <1v<2^>2^> 1v1+ 1v1- 8$!", "5, 1024", "1024=5,6,7,8,9");
		HashMap<Integer, Integer> m = HackVM.parseMemory( "1024=9,8,7,6,5" );
		assertNull( result.getError() );
		assertEquals( result.getMemory(), m );
	}

}
