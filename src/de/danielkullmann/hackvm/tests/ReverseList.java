package de.danielkullmann.hackvm.tests;

import java.util.Observable;
import java.util.Observer;

import de.danielkullmann.hackvm.HackVM;
import de.danielkullmann.hackvm.HackVM.Trace;
import de.danielkullmann.hackvm.VMState;

public class ReverseList implements Observer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new ReverseList().run();
	}

	private void run() {
		HackVM vm = new HackVM();
		//          setup   loop loop end?      copy one
		vm.addObserver( this );
		VMState result = vm.execute("0^2v+1- 1^1^ 1^1^:0^1-*55*? <1v<2^>2^> 1v1+ 1v1- 8$!", "5, 1024", "1024=5,6,7,8,9");
		System.out.println( result.getError() );
		System.out.println( result.getMemory() );
	}

	@Override
	public void update(Observable o, Object arg) {
		VMState state = (VMState) arg;
		Trace t = state.getExecutionTrace().get( state.getExecutionTrace().size()-1 );
		System.out.println( t );
		if ( state.getTicks() > 130 ) System.exit(1);
	}

}
