package de.danielkullmann.hackvm.tests;

import junit.framework.TestCase;
import de.danielkullmann.hackvm.ExampleExercises;
import de.danielkullmann.hackvm.ExercisePlusOneSolution;

public class Test2 extends TestCase {

	public void test2() {
		ExampleExercises exercises = new ExampleExercises();
		
		for (ExercisePlusOneSolution exercise : exercises.getExercises()) {
			assertFalse( exercise.checkProgram( "" ) );
			assertTrue( exercise.getSolution(), exercise.checkProgram( exercise.getSolution() ) );
		}
	}
}
