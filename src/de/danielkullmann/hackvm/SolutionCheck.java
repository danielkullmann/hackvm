package de.danielkullmann.hackvm;

public interface SolutionCheck {

	public abstract boolean check(String program);

}
