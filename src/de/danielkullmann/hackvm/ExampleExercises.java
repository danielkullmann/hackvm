package de.danielkullmann.hackvm;

import java.util.ArrayList;
import java.util.List;

public class ExampleExercises {

	public List<ExercisePlusOneSolution> getExercises() {
		ArrayList<ExercisePlusOneSolution> result = new ArrayList<ExercisePlusOneSolution>();

		{
			String d1 = "Reverse list of ints that starts at memory position s0, and that has s1 elements";
			List<SolutionCheck> scl = new ArrayList<SolutionCheck>();
			String stack = "5, 1024";
			String memory = "1024=5,6,7,8,9";
			String output = "";
			String endStack = "";
			String endMemory = "1024=9,8,7,6,5";
			ManualSolutionCheck sc1 = new ManualSolutionCheck(stack, memory, output, endStack, endMemory );
			scl.add(sc1 );
		    String program = "0^2v+1- 1^1^ 1^1^:0^1-*55*? <1v<2^>2^> 1v1+ 1v1- 8$!"; 
			ExercisePlusOneSolution e1 = new ExercisePlusOneSolution(d1, scl, "", program );
			result.add( e1 );
		}

		{
			String d1 = "Output the first s0 Fibonacci numbers, separated by space (Ascii 32)";
			List<SolutionCheck> scl = new ArrayList<SolutionCheck>();
			ManualSolutionCheck sc1 = new ManualSolutionCheck("5", "", "1 1 2 3 5", "", "" );
			scl.add(sc1 );
			// TODO Dummy solution!!
			ExercisePlusOneSolution e1 = new ExercisePlusOneSolution(d1, scl, "", "1p 48*P 1p 48*P 2p 48*P 3p 48*P 5p" );
			result.add( e1 );
		}

		{
			String d2 = "Add the top three stack elements together";
			ArrayList<SolutionCheck> scl = new ArrayList<SolutionCheck>();
			String stack = "4, 2, -7";
			String memory = "";
			String output = "";
			String endStack = "-1";
			String endMemory = "";
			ManualSolutionCheck sc2 = new ManualSolutionCheck(stack, memory, output, endStack, endMemory );
			scl.add(sc2 );
			ExercisePlusOneSolution e2 = new ExercisePlusOneSolution(d2, scl, "", "++" );
			result.add( e2 );
		}
		
		return result;
	}
}
