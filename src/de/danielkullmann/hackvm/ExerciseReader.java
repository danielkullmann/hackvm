package de.danielkullmann.hackvm;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ExerciseReader extends DefaultHandler {

	private static final String TAG_EXERCISES = "exercises";
	private static final String TAG_EXERCISE = "exercise";
	private static final String TAG_CATEGORY = "category";
	private static final String TAG_SOLUTION = "solution";
	private static final String TAG_DESCRIPTION = "description";
	private static final String TAG_STACK = "stack";
	private static final String TAG_MEMORY = "memory";
	private static final String TAG_END_STACK = "end-stack";
	private static final String TAG_END_MEMORY = "end-memory";
	private static final String TAG_OUTPUT = "output";
	private static final String ATTR_ID = "id";
	private static final String ATTR_ILLEGAL_OPCODES = "illegal-opcodes";
	private static final String ATTR_SOLUTION_CHECK_CLASS = "class";
	
	private enum State {NONE, EXERCISE, SOLUTION, DESCRIPTION, MEMORY, STACK, END_MEMORY, END_STACK, OUTPUT, CATEGORY };

	private Stack<State> state = new Stack<State>();
	
	private List<Exercise> exercises = new ArrayList<Exercise>();

	private Exercise exercise = null;
	private ManualSolutionCheck solutionCheck = null;
	private StringBuilder charValue = null;
	private String solutionCheckClass = null;
	private int categoryId;

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (qName.equals( TAG_EXERCISES ) ) {
			// do nothing
		} 
		else if (qName.equals( TAG_EXERCISE ) ) {
			exercises.add( exercise );
			exercise = null;
			state.pop();
		} 
		else if (qName.equals( TAG_SOLUTION ) ) {
			if (solutionCheckClass != null ) {
				try {
					exercise.getSolutionChecks().add( (SolutionCheck) Class.forName( solutionCheckClass ).newInstance() );
				} catch (Exception e) {
					HackUtil.e( "solutionCheckClass can't be instantiated", e );
				}
			} else {
				exercise.getSolutionChecks().add( solutionCheck  );
			}
			solutionCheck = null;
			solutionCheckClass = null;
			state.pop();
		}
		else if ( qName.equals( TAG_DESCRIPTION ) ) {
			exercise.setDescription( charValue.toString() );
			charValue = null;
			state.pop();
		}
		else if ( qName.equals( TAG_CATEGORY ) ) {
			exercise.setCategoryId( categoryId );
			exercise.setCategoryName( charValue.toString() );
			categoryId = -1;
			charValue = null;
			state.pop();
		}
		else if ( qName.equals( TAG_MEMORY ) ) {
			solutionCheck.setMemory( HackVM.parseMemory( charValue.toString() ) );
			charValue = null;
			state.pop();
		}
		else if ( qName.equals( TAG_STACK ) ) {
			solutionCheck.setStack( HackVM.parseStack( charValue.toString() ) );
			charValue = null;
			state.pop();
		}
		else if ( qName.equals( TAG_END_MEMORY ) ) {
			solutionCheck.setEndMemory( HackVM.parseMemory( charValue.toString() ) );
			charValue = null;
			state.pop();
		}
		else if ( qName.equals( TAG_END_STACK ) ) {
			solutionCheck.setEndStack( HackVM.parseStack( charValue.toString() ) );
			charValue = null;
			state.pop();
		}
		else if ( qName.equals( TAG_OUTPUT ) ) {
			solutionCheck.setOutput( charValue.toString() );
			charValue = null;
			state.pop();
		} 
		else {
			HackUtil.d( "unknown tag: " + qName );
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		if ( qName.equals( TAG_EXERCISES ) ) {
			// do nothing..
		}
		else if (qName.equals( TAG_EXERCISE ) ) {
			exercise = new Exercise(null, new ArrayList<SolutionCheck>(), null);
			exercise.setId( attributes.getValue( ATTR_ID ) );
			exercise.setIllegalOpcodes( attributes.getValue( ATTR_ILLEGAL_OPCODES ) );
			state.push( State.EXERCISE );
		}
		else if (qName.equals( TAG_SOLUTION ) ) {
			solutionCheckClass = attributes.getValue( ATTR_SOLUTION_CHECK_CLASS );
			if ( solutionCheckClass == null ) {
				solutionCheck = new ManualSolutionCheck((String)null, null, null, null, null );
			}
			state.push( State.SOLUTION );
		}
		else if (qName.equals( TAG_DESCRIPTION ) ) {
			if ( state.peek() != State.EXERCISE ) throw new IllegalArgumentException( qName + " tag only allowed inside exercise tag");
			charValue = new StringBuilder();
			state.push( State.DESCRIPTION );
		} 
		else if (qName.equals( TAG_CATEGORY ) ) {
			if ( state.peek() != State.EXERCISE ) throw new IllegalArgumentException( qName + " tag only allowed inside exercise tag");
			try {
				categoryId = Integer.parseInt( attributes.getValue( ATTR_ID ) );
			} catch (NumberFormatException e) {
				categoryId = -1;
			}
			charValue = new StringBuilder();
			state.push( State.CATEGORY );
		} 
		else if (qName.equals( TAG_MEMORY ) ) {
			if ( state.peek() != State.SOLUTION ) throw new IllegalArgumentException( TAG_MEMORY + " tag only allowed inside solution tag");
			charValue = new StringBuilder();
			state.push( State.MEMORY );
		} 
		else if (qName.equals( TAG_STACK ) ) {
			if ( state.peek() != State.SOLUTION ) throw new IllegalArgumentException( TAG_STACK + " tag only allowed inside solution tag");
			charValue = new StringBuilder();
			state.push( State.STACK );
		} 
		else if (qName.equals( TAG_END_MEMORY ) ) {
			if ( state.peek() != State.SOLUTION ) throw new IllegalArgumentException( TAG_END_MEMORY + " tag only allowed inside solution tag");
			charValue = new StringBuilder();
			state.push( State.END_MEMORY );
		} 
		else if (qName.equals( TAG_END_STACK ) ) {
			if ( state.peek() != State.SOLUTION ) throw new IllegalArgumentException( TAG_END_STACK + " tag only allowed inside solution tag");
			charValue = new StringBuilder();
			state.push( State.END_STACK );
		} 
		else if (qName.equals( TAG_OUTPUT ) ) {
			if ( state.peek() != State.SOLUTION ) throw new IllegalArgumentException( TAG_OUTPUT + " tag only allowed inside solution tag");
			charValue = new StringBuilder();
			state.push( State.OUTPUT );
		} 
		else {
			HackUtil.d( "unknown tag " + qName + ":" + qName );
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		if ( charValue != null ) {
			charValue.append( new String(ch).substring(start, start+length) );
		}
	}

	public List<Exercise> getExercises() {
		return exercises;
	}

}
