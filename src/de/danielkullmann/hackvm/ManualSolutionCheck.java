package de.danielkullmann.hackvm;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Stack;


public class ManualSolutionCheck implements SolutionCheck {

	private Stack<Integer> stack;
	private String output;
	private HashMap<Integer, Integer> memory;
	private Stack<Integer> endStack;
	private HashMap<Integer, Integer> endMemory;

	public ManualSolutionCheck(Stack<Integer> stack,
			HashMap<Integer, Integer> memory, String output,
			Stack<Integer> endStack, HashMap<Integer, Integer> endMemory) {
		super();
		this.stack = stack;
		this.memory = memory;
		this.output = output;
		this.endStack = endStack;
		this.endMemory = endMemory;
	}

	public ManualSolutionCheck(String stack, String memory, String output,
			String endStack, String endMemory) {
		this.stack = HackVM.parseStack(stack);
		this.memory = HackVM.parseMemory(memory);
		this.output = output;
		this.endStack = HackVM.parseStack(endStack);
		this.endMemory = HackVM.parseMemory(endMemory);
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean check(String program) {
		HackVM vm = new HackVM();
		VMState result = vm.execute(program, (Stack<Integer>) stack.clone(),
				(HashMap<Integer, Integer>) memory.clone());
		if (!result.getOutput().equals(output)) {
			System.out.println(output + " != " + result.getOutput());
			return false;
		}
		int stackDiff = result.getStack().size() - endStack.size();
		if (stackDiff < 0) {
			System.out.println("stack problem");
			return false;
		}
		int stLen = result.getStack().size();
		if (!result.getStack().subList(stackDiff, stLen).equals(endStack)) {
			System.out.println("stack diff");
			System.out.println( result.getStack() );
			System.out.println( endStack );
			return false;
		}
		for (Entry<Integer, Integer> entry : endMemory.entrySet()) {
			if (!result.getMemory().containsKey(entry.getKey())) {
				System.out.println( "memory key error" );
				System.out.println( result.getMemory() );
				System.out.println( endMemory );
				return false;
			}
			if (!result.getMemory().get(entry.getKey()).equals(entry.getValue())) {
				System.out.println( "memory value error " + entry.getKey() + " " + result.getMemory().get(entry.getKey()) + " != " + entry.getValue() );
				System.out.println( result.getMemory() );
				System.out.println( endMemory );
				return false;
			}

		}
		return true;
	}

	public Stack<Integer> getStack() {
		return stack;
	}

	public String getOutput() {
		return output;
	}

	public HashMap<Integer, Integer> getMemory() {
		return memory;
	}

	public Stack<Integer> getEndStack() {
		return endStack;
	}

	public HashMap<Integer, Integer> getEndMemory() {
		return endMemory;
	}

	public void setStack(Stack<Integer> stack) {
		this.stack = stack;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public void setMemory(HashMap<Integer, Integer> memory) {
		this.memory = memory;
	}

	public void setEndStack(Stack<Integer> endStack) {
		this.endStack = endStack;
	}

	public void setEndMemory(HashMap<Integer, Integer> endMemory) {
		this.endMemory = endMemory;
	}

}
