package de.danielkullmann.hackvm;

import java.util.List;

public class ExercisePlusOneSolution extends Exercise {

	public ExercisePlusOneSolution(String description,List<SolutionCheck> solutionChecks, String illegalOpcodes, String solution) {
		super(description, solutionChecks, illegalOpcodes);
		this.solution = solution;
	}

	private String solution;

	public String getSolution() {
		return solution;
	}
	
}
