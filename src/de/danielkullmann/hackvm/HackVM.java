package de.danielkullmann.hackvm;

import java.util.HashMap;
import java.util.Observable;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * Instruction	Description:
 * ' '	Do Nothing
 * 'p'	Print S0 interpreted as an integer
 * 'P'	Print S0 interpreted as an ASCII character (only the least significant 7 bits of the value are used)
 * '0'	Push the value 0 on the stack
 * '1'	Push the value 1 on the stack
 * '2'	Push the value 2 on the stack
 * '3'	Push the value 3 on the stack
 * '4'	Push the value 4 on the stack
 * '5'	Push the value 5 on the stack
 * '6'	Push the value 6 on the stack
 * '7'	Push the value 7 on the stack
 * '8'	Push the value 8 on the stack
 * '9'	Push the value 9 on the stack
 * '+'	Push S1+S0
 * '-'	Push S1-S0
 * '*'	Push S1*S0
 * '/'	Push S1/S0
 * ':'	Push -1 if S1<S0, 0 if S1=S0, or 1 S1>S0
 * 'g'	Add S0 to the program counter
 * '?'	Add S0 to the program counter if S1 is 0
 * 'c'	Push the program counter on the call stack and set the program counter to S0
 * '$'	Set the program counter to the value pop'ed from the call stack
 * '<'	Push the value of memory cell S0
 * '>'	Store S1 into memory cell S0
 * '^'	Push a copy of S<S0+1> (ex: 0^ duplicates S0)
 * 'v'	Remove S<S0+1> from the stack and push it on top (ex: 1v swaps S0 and S1)
 * 'd'	Drop S0
 * '!'	Terminate the program
 * 
 * @author daku
 */
public class HackVM extends Observable {

	private static final int MAX_TICKS = 10000;
	private String sideEffect;

	
	private static HashMap<Character, Command> commands = new HashMap<Character, Command>();

	private enum Result { Normal, Jump, Exit};
	
	public VMState execute(String program, String stackContent, String memoryContent ) {
		Stack<Integer> stack = parseStack(stackContent);
		HashMap<Integer, Integer> memory = parseMemory(memoryContent);
		return execute( program, stack, memory );
	}

	@SuppressWarnings("unchecked")
	public VMState execute(String program, Stack<Integer> stack, HashMap<Integer, Integer> memory) {
		VMState result = new VMState();
		if ( stack != null ) result.setStack( (Stack<Integer>) stack.clone() );
		if ( memory != null ) result.setMemory( (HashMap<Integer, Integer>) memory.clone() );

		while (true) {
			if (result.getProgramCounter() < 0) {
				result.setError( "programCounter < 0" );
				return result;
			}
			if (result.getProgramCounter() >= program.length()) {
				result.setError( "programCounter < programLength" );
				return result;
			}

			char c = program.charAt(result.getProgramCounter());

			Command cmd = commands.get(c);
			if (cmd == null) {
				result.setError( "illegal opcode '" + c + "'" );
				return result;
			}

			sideEffect = "";
			int pc = result.getProgramCounter();
			Result r = cmd.execute(result);
			Trace trace = new Trace(c, pc, result.topStack(), sideEffect);
			result.addTrace( trace );
			setChanged();
			notifyObservers( result );
				
			switch ( r ) {

			case Normal:
				result.setProgramCounter( result.getProgramCounter() + 1 );
				if (result.getProgramCounter() == program.length()) {
					return result;
				}
				break;

			case Jump:
				break;

			case Exit:
				return result;
			}

			result.tick();
			if ( result.getTicks() > MAX_TICKS ) {
				result.setError( "program runs too long" );
				return result;
			}
		}
	}


	private abstract class Command {
		abstract public Result execute(VMState state);
	}

	public HackVM() {
		commands.put(' ', new Command() {
			@Override
			public Result execute(VMState state) {
				// don't do anything
				return Result.Normal;
			}
		});

		commands.put('p', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer top = state.getStack().pop();
				state.output(top);
				sideEffect = "print " + top ;
				return Result.Normal;
			}
		});

		commands.put('P', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer top = state.getStack().pop();
				Character ch = new Character((char) (top & 0x7f));
				state.output(ch);
				sideEffect = "print " + ch;
				return Result.Normal;
			}
		});

		for (int i = 0; i < 10; i++) {
			final int v = i;
			commands.put((char) ('0' + v), new Command() {
				@Override
				public Result execute(VMState state) {
					state.getStack().add(v);
					return Result.Normal;
				}
			});
		}

		commands.put('+', new Command() {
			@Override
			public Result execute(VMState state) {
				state.getStack().push(state.getStack().pop() + state.getStack().pop());
				return Result.Normal;
			}
		});

		commands.put('-', new Command() {
			@Override
			public Result execute(VMState state) {
				state.getStack().push(-(state.getStack().pop() - state.getStack().pop()));
				return Result.Normal;
			}
		});

		commands.put('*', new Command() {
			@Override
			public Result execute(VMState state) {
				state.getStack().push(state.getStack().pop() * state.getStack().pop());
				return Result.Normal;
			}
		});

		commands.put('/', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				Integer s1 = state.getStack().pop();
				state.getStack().push(s1 / s0);
				return Result.Normal;
			}
		});

		// ':' Push -1 if S1<S0, 0 if S1=S0, or 1 S1>S0
		commands.put(':', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				Integer s1 = state.getStack().pop();
				if ( s1 < s0 ) state.getStack().push( -1 );
				else if ( s1 > s0 ) state.getStack().push( 1 );
				else state.getStack().push( 0 );
				return Result.Normal;
			}
		});

		// 'g' Add S0 to the program counter
		commands.put('g', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				state.setProgramCounter( state.getProgramCounter() + s0 );
				return Result.Jump;
			}
		});
		
		// '?' Add S0 to the program counter if S1 is 0
		commands.put('?', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				Integer s1 = state.getStack().pop();
				if ( s1 == 0 ) {
					state.setProgramCounter( state.getProgramCounter() + s0 );
					return Result.Jump;
				}
				return Result.Normal;
			}
		});

		// 'c' Push the program counter on the call stack and set the program counter to S0
		commands.put('c', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				int pc = state.getProgramCounter();
				state.setProgramCounter( s0 );
				state.getStack().push( pc );
				return Result.Jump;
			}
		});

		// '$' Set the program counter to the value pop'ed from the call stack
		commands.put('$', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				state.setProgramCounter( s0 );
				return Result.Jump;
			}
		});
		
		// '<' Push the value of memory cell S0
		commands.put('<', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				Integer mem = state.getMemory().get( s0 );
				if ( mem == null ) mem = 0;
				state.getStack().push( mem );
				return Result.Normal;
			}
		});
		
		// '>' Store S1 into memory cell S0
		commands.put('>', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				Integer s1 = state.getStack().pop();
				state.getMemory().put( s0, s1 );
				sideEffect = "mem[" + s0 + "] = " + s1;
				return Result.Normal;
			}
		});
		
		// '^' Push a copy of S<S0+1> (ex: 0^ duplicates S0)
		commands.put('^', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				state.getStack().push( state.getStack().get( state.getStack().size() - 1 - s0 ) );
				return Result.Normal;
			}
		});
		
		// 'v' Remove S<S0+1> from the stack and push it on top (ex: 1v swaps S0 and S1)
		commands.put('v', new Command() {
			@Override
			public Result execute(VMState state) {
				Integer s0 = state.getStack().pop();
				Integer val = state.getStack().remove( state.getStack().size() - 1 - s0 );
				state.getStack().push( val );
				return Result.Normal;
			}
		});
		
		// 'd' Drop S0
		commands.put('d', new Command() {
			@Override
			public Result execute(VMState state) {
				state.getStack().pop();
				return Result.Normal;
			}
		});
		
		// '!' Terminate the program
		commands.put('!', new Command() {
			@Override
			public Result execute(VMState state) {
				sideEffect = "exit";
				return Result.Exit;
			}
		});
	}

	public static Stack<Integer> parseStack(String stackContent) {
		Stack<Integer> stack = new Stack<Integer>();
		if ( stackContent == null || stackContent.equals("") ) {
			return stack;
		}
		StringTokenizer tokenizer = new StringTokenizer(stackContent, ", ");
		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (!"".equals(token)) {
				stack.add(Integer.parseInt(token));
			}
		}
		return stack;
	}

	public static HashMap<Integer, Integer> parseMemory(String memoryContent) {
		HashMap<Integer, Integer> memory = new HashMap<Integer, Integer>();
		if ( memoryContent == null ) return memory;
		StringTokenizer tokenizer1 = new StringTokenizer(memoryContent, ";");
		while (tokenizer1.hasMoreTokens()) {
			String token1 = tokenizer1.nextToken();
			if (!"".equals(token1)) {
				int idx = token1.indexOf('=');
				if ( idx < 0 ) throw new IllegalArgumentException( "part '" + token1 + "' contains no '='");
				int startIndex = Integer.parseInt( token1.substring(0, idx) );
				StringTokenizer tokenizer2 = new StringTokenizer(token1.substring(idx+1), ", ");
				while (tokenizer2.hasMoreTokens()) {
					String token2 = tokenizer2.nextToken();
					memory.put( startIndex, Integer.parseInt( token2 ) );
					startIndex++;
				}
			}
		}
		return memory;
	}

	public static class Trace {
		private char opcode;
		private int programCounter;
		private Stack<Integer> stack;
		private String sideEffect;
		
		public Trace(char opcode, int programCounter, Stack<Integer> stack, String sideEffect) {
			super();
			this.opcode = opcode;
			this.programCounter = programCounter;
			this.stack = stack;
			this.sideEffect = sideEffect;
		}

		public int getProgramCounter() {
			return programCounter;
		}

		public Stack<Integer> getStack() {
			return stack;
		}

		public String getSideEffect() {
			return sideEffect;
		}

		@Override
		public String toString() {
			String stackStr = "";
			for (Integer elem : stack) {
				stackStr += elem + " ";
			}
			return "Trace: pc=" + programCounter + ", opcode=[" + opcode + "], stack="
					+ stackStr  + " [" + sideEffect + "]";
		}
		
	}

}
